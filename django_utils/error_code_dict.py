import logging.config

err_dict = {
    "CommandError": [11, "Django command error"],
    "DoesNotExist": [15, "Requested element does not exist"],
    "EmptyListError": [3, "Fetching returned no results"],
    "IndexError": [18, ""],
    "ImportError": [13, "Could not import Django"],
    "InvalidClientError": [7, "Client used is stale or invalid"],
    "InvalidPayloadError": [5, "Returned payload is invalid"],
    "InvalidStatusCodeError": [4, "The returned status code is invalid"],
    "KeyError": [14, "Error initializing the fetcher"],
    "OverflowError": [16, ""],
    "TokenExpiredError": [6, "Token has expired"],
    "TypeError": [14, ""],
    "RuntimeError": [1, "Runtime error"],
    "ValidationError": [19, ""],
    "ValueError": [10, "Didn't succeed fetching data"]
}


def err_dict_func(exc, msg="", log_attributes=[]):
    """Merge an exception, handler attributes and a message into a log message"""
    if log_attributes:
        provider_city, provider_name, api_version, saver_type, fetch_timerange, start_timerange, stop_timerange, historical = (
            log_attributes
        )
    exc_name = exc.__class__.__name__
    if exc_name in err_dict:
        err_code, err_desc = err_dict[exc_name]
    else:
        err_code = "0"
        err_desc = "unknown exception"
    log_msg = '{}","error code": "{}", "description": "{}", "raised message": "{}'.format(
        str(exc).replace('"', "'"), err_code, err_desc, msg
    )
    if log_attributes:
        log_msg = (
            '{}", "provider_city": "{}", "provider_name": "{}", "api_version": "{}", "saver_type": "{}", "fetch_timerange": "{}", "start_timerange": "{}",'
            '"stop_timerange": "{}", "historical": "{}'.format(
                log_msg,
                provider_city,
                provider_name,
                api_version,
                saver_type,
                fetch_timerange,
                start_timerange,
                stop_timerange,
                historical,
            )
        )
    return log_msg.replace('"', "*double_quote*")


def log_error(logger, exc, msg="", log_attributes=[]):
    logger.warning(f"{err_dict_func(exc,msg,log_attributes)}")
    logger.debug(f"{err_dict_func(exc,msg,log_attributes)}", exc_info=True)


class LoggingFormatter(logging.Formatter):
    """Formatter used for the log message and the exception traceback"""

    def formatException(self, exc_info):
        result = super(LoggingFormatter, self).formatException(exc_info)
        result = result.replace('"', "'")
        result = result.replace("\n", r"\n")
        result = f"*double_quote*{result}*double_quote*"
        return result

    def format(self, record):
        s = super(LoggingFormatter, self).format(record)
        s = s.replace('"', "'")
        s = s.replace("*double_quote*", '"')
        if record.exc_text:
            s = f"{s}}},"
        else:
            s = f'{s}""}},'
        return s
